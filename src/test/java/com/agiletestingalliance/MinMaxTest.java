package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testCompareScenario1(){
        int maxNumber= new MinMax().findGreater(4,3);
        assertEquals("Add", 4, maxNumber);

    }
   @Test
    public void testCompareScenario2() throws Exception {

        int minNumber= new MinMax().findGreater(3,4);
        assertEquals("Add", 4, minNumber);

    }
    @Test
    public void testCompareScenario3() throws Exception {

        String returnedStringIfNotEmpty= new MinMax().bar("roopanshu");
        assertEquals("bar", "roopanshu", returnedStringIfNotEmpty);

        String returnedStringIfEmpty= new MinMax().bar("");
        assertEquals("bar", "", returnedStringIfEmpty);

	String returnedStringIfNull = new MinMax().bar(null);
	assertEquals("bar", null, returnedStringIfNull);
    }

     
}

